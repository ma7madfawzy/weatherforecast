package fawzy.com.weathernavigator.ui.base;

import android.arch.lifecycle.ViewModel;

import io.reactivex.disposables.CompositeDisposable;


/**
 * Created by fawzy on 1/25/19.
 */

public class BaseVM extends ViewModel {
    protected CompositeDisposable compositeDisposable;

    public BaseVM() {
        compositeDisposable = new CompositeDisposable();
    }

    public void clearSubscriptions() {
        compositeDisposable.clear();
    }


}
