package fawzy.com.weathernavigator.ui.home.main;


import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.google.android.gms.maps.model.LatLng;

import javax.inject.Inject;

import fawzy.com.weathernavigator.AppClass;
import fawzy.com.weathernavigator.R;
import fawzy.com.weathernavigator.constants.Constants;
import fawzy.com.weathernavigator.data.cache.CacheManger;
import fawzy.com.weathernavigator.data.services.UserService;
import fawzy.com.weathernavigator.databinding.HomeActivityBinding;
import fawzy.com.weathernavigator.ui.base.BaseActivity;
import fawzy.com.weathernavigator.ui.home.map.MapFragment;
import fawzy.com.weathernavigator.utils.AnimationUtils;
import fawzy.com.weathernavigator.utils.LocalityUtil;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;

/**
 * Created by fawzy on 1/25/19.
 */

public class HomeActivity extends BaseActivity implements MapFragment.OnPickLocation, ViewPager.OnPageChangeListener, HomeHandler {
    @Inject
    UserService iUserService;
    @Inject
    SharedPreferences preferences;
    private HomeActivityBinding binding;
    private HomeModel model;
    private HomeVM viewModel;

    public static void start(Context context) {
        Intent intent = new Intent(context, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDI();
        setLocality(preferences);
        renderView();
        setViewModel();
    }

    private void setViewModel() {
        viewModel = ViewModelProviders.of(this).get(HomeVM.class);
        viewModel.set(this, iUserService, model, model.getResource());
    }


    private void initDI() {
        ((AppClass) getApplication()).getAppComponent().inject(this);
    }

    protected void renderView() {
        binding = DataBindingUtil.setContentView(this, R.layout.home_activity);
        model = new HomeModel();
        setModel();
        setUpToolbar(binding.appBar.toolbar, R.string.app_name, false);
        binding.appBar.languageTV.setOnClickListener(this::onChangeLanguageClicked);
        setViewPagerAdapter();
    }

    private void setModel() {
        model.setResource(CacheManger.getInstance().getCachedData());
        //setting initial latLng as Cairo
        model.setPickedLatLng(new LatLng(30.0444, 31.2357));
        binding.setModel(model);
    }

    private void setViewPagerAdapter() {
        PagerAdapter adapter = new PagerAdapter(this, getSupportFragmentManager(), model.getPickedLatLng());
        // Set the adapter onto the view pager
        binding.viewpager.setAdapter(adapter);
        binding.viewpager.setCurrentItem(PagerAdapter.bookmarkIndex, true);
        binding.slidingTabs.setupWithViewPager(binding.viewpager);
        binding.viewpager.addOnPageChangeListener(this);
        for (int i = 0; i < binding.slidingTabs.getTabCount(); i++) {
            binding.slidingTabs.getTabAt(i).setCustomView(adapter.getTabView(i));
        }
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    @Override
    public void onLocationPicked(LatLng location) {
        model.setPickedLatLng(location);
    }

    public void onAddBookmarkClicked(View view) {
        viewModel.getWeatherData(model.getPickedLatLng());
    }

    public void onChangeLanguageClicked(View view) {
        LocalityUtil.changeLanguage(this, preferences, Constants.isArabic ? "en" : "ar");
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        model.setMapShown(position == PagerAdapter.mapIndex);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void deleteItem(int position) {
        viewModel.deleteItem(position);
    }

    @Override
    public void openMap() {
        binding.viewpager.setCurrentItem(PagerAdapter.mapIndex, true);
        AnimationUtils.getInstance().shakeAnimation(binding.fab, 700);
    }

    @Override
    public void onItemAdded() {
        binding.viewpager.setCurrentItem(PagerAdapter.bookmarkIndex);
    }

}


