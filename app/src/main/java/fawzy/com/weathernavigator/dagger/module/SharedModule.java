package fawzy.com.weathernavigator.dagger.module;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(includes = {ContextModule.class})
public class SharedModule {
    @Provides
    @Singleton
    SharedPreferences provideSharedPreferences(@Named("context") Context ctx) {
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }

    @Provides
    SharedPreferences.Editor providePreferencesEditor(SharedPreferences preferences) {
        return preferences.edit();
    }
}
