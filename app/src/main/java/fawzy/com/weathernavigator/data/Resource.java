package fawzy.com.weathernavigator.data;

/**
 * Created by Fawzy on  1/25/19.
 */

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

// A generic class that contains data and status about loading this data.
public class Resource<T> extends BaseObservable {
    @NonNull
    private Status status;
    @Nullable
    private T data;
    @Nullable
    private String message;

    private Resource(@NonNull Status status, @Nullable T data,
                     @Nullable String message) {
        this.status = status;
        this.data = data;
        this.message = message;
    }

    public static <T> Resource<T> success(@NonNull T data) {
        return new Resource<>(Status.SUCCESS, data, null);
    }

    public static <T> Resource<T> error(String msg, @Nullable T data) {
        return new Resource<>(Status.ERROR, data, msg);
    }

    public static <T> Resource<T> loading(@Nullable T data) {
        return new Resource<>(Status.LOADING, data, null);
    }

    public static <T> Resource<T> empty(@Nullable T data) {
        return new Resource<>(Status.EMPTY_DATA, data, null);
    }

    @NonNull
    @Bindable
    public Status getStatus() {
        return status;
    }

    public void setStatus(@NonNull Status status) {
        this.status = status;
    }

    @Nullable
    @Bindable
    public String getMessage() {
        return message;
    }

    @Nullable
    public T getData() {
        return data;
    }

    public void setData(@Nullable T data) {
        this.data = data;
    }
}
