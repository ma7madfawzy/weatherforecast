package fawzy.com.weathernavigator.utils;

import android.view.View;
import android.view.animation.DecelerateInterpolator;

import static fawzy.com.weathernavigator.utils.ContextUtil.getScreenHeight;


/**
 * Created by fawzy on 1/25/19.
 */

public class RecyclerItemAnimator {
    public static void runEnterAnimation(View view, int currentPosition, int lastAnimated) {
        if (currentPosition < lastAnimated) {
            view.setTranslationY(getScreenHeight(view.getContext()));
            view.animate().translationY(0)
                    .setInterpolator(new DecelerateInterpolator(3.f))
                    .setDuration(700)
                    .start();
        }
    }

    public static void runEnterAnimation(View view, int currentPosition, int lastVisible, int lastAnimated) {
        if (currentPosition < lastAnimated && currentPosition < lastVisible) {
            view.setTranslationY(getScreenHeight(view.getContext()));
            view.animate().translationY(0)
                    .setInterpolator(new DecelerateInterpolator(3.f))
                    .setDuration(700)
                    .start();
        }
    }
}
