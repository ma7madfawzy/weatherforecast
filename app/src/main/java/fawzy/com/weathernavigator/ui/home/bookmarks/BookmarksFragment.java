package fawzy.com.weathernavigator.ui.home.bookmarks;


import android.arch.lifecycle.ViewModelProviders;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;

import java.util.ArrayList;

import javax.inject.Inject;

import fawzy.com.weathernavigator.AppClass;
import fawzy.com.weathernavigator.R;
import fawzy.com.weathernavigator.data.cache.CacheManger;
import fawzy.com.weathernavigator.data.services.UserService;
import fawzy.com.weathernavigator.databinding.FragmentBookmarksBinding;
import fawzy.com.weathernavigator.ui.base.BaseFragment;
import fawzy.com.weathernavigator.ui.home.main.HomeHandler;
import fawzy.com.weathernavigator.ui.home.main.HomeVM;

/**
 * A simple {@link Fragment} subclass.
 */
public class BookmarksFragment extends BaseFragment {
    FragmentBookmarksBinding binding;
    @Inject
    Gson gson;
    @Inject
    UserService iService;
    @Inject
    SharedPreferences preferences;
    private HomeVM viewModel;
    private BookmarksModel model;
    private BookmarksAdapter adapter;

    public BookmarksFragment() {
        // Required empty public constructor
    }

    public static BookmarksFragment getInstance() {
        BookmarksFragment fragment = new BookmarksFragment();
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDI();
    }

    private void initDI() {
        ((AppClass) getActivity().getApplication()).getAppComponent().inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        renderView(inflater, container);
        setViewModel();
        return binding.getRoot();
    }

    protected void renderView(LayoutInflater inflater, ViewGroup container) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_bookmarks, container, false);
        model = new BookmarksModel();
        binding.setModel(model);
        setRecycler();
        initListeners();
    }

    private void setRecycler() {
        adapter = new BookmarksAdapter(new ArrayList<>(), getActivity());
        binding.recycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        binding.recycler.setAdapter(adapter);
    }

    protected void setViewModel() {
        viewModel = ViewModelProviders.of(getActivity()).get(HomeVM.class);
        observeData();
    }

    private void observeData() {
        viewModel.getDataList().observe(this, listResource -> {
            model.setResource(listResource);
            adapter.setDataList(listResource.getData());
            CacheManger.getInstance().cacheData(listResource);
        });
    }


    private void initListeners() {
        binding.noDataContainer.parentLayout.setOnClickListener(v -> ((HomeHandler) getActivity()).openMap());
    }
}
