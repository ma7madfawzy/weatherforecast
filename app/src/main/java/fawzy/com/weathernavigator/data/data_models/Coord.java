package fawzy.com.weathernavigator.data.data_models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by fawzy on 1/25/19.
 */

public class Coord implements Parcelable {

    @SuppressWarnings("unused")
    public static final Creator<Coord> CREATOR = new Creator<Coord>() {
        @Override
        public Coord createFromParcel(Parcel in) {
            return new Coord(in);
        }

        @Override
        public Coord[] newArray(int size) {
            return new Coord[size];
        }
    };
    private float lon;
    private float lat;

    public Coord() {
    }

    private Coord(Parcel in) {
        lon = in.readFloat();
        lat = in.readFloat();
    }

    public float getLon() {
        return lon;
    }

    public void setLon(float lon) {
        this.lon = lon;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeFloat(lon);
        dest.writeFloat(lat);
    }

}
