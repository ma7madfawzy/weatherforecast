package fawzy.com.weathernavigator.constants;

/**
 * Created by Fawzy on 1/25/19
 */
public class ServicesConstants {
    public static final String BASE_URL = "http://api.openweathermap.org/data/2.5/";
    public static final String IMAGE_BASE_URL = "http://openweathermap.org/img/w/";
}
