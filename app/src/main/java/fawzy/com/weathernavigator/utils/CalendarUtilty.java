package fawzy.com.weathernavigator.utils;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static java.util.Locale.ENGLISH;

/**
 * by Adel on 8/19/2016.
 */
public class CalendarUtilty {

    public static String GetFormattedGeorgianDate(String georgianDate, Locale locale, boolean withDayOfWeek) throws ParseException {
        try {
            DateFormat fromFormat = new SimpleDateFormat("dd/MM/yyyy");
            fromFormat.setLenient(false);

            DateFormat toFormat = withDayOfWeek ? new SimpleDateFormat("EEE, dd MMM yyyy") : new SimpleDateFormat("dd MMM yyyy");
            toFormat.setLenient(false);
            Date date = fromFormat.parse(georgianDate);
            return toFormat.format(date);
        } catch (Exception e) {
            return GetFormattedGeorgianDate2(georgianDate, locale, withDayOfWeek);
        }
    }

    public static String GetFormattedGeorgianDate2(String georgianDate, Locale locale, boolean withDayOfWeek) throws ParseException {
        DateFormat fromFormat = new SimpleDateFormat("M/dd/yyyy");
        fromFormat.setLenient(false);

        DateFormat toFormat = withDayOfWeek ? new SimpleDateFormat("EEE, dd MMM yyyy") : new SimpleDateFormat("dd MMM yyyy");
        toFormat.setLenient(false);
        Date date = fromFormat.parse(georgianDate);
        return toFormat.format(date);
    }

    public static String GetCurrentTimestamp() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("EEE MMM dd kk:mm:ss zXXX yyyy", new Locale("en"));//dd/MM/yyyy
        Date now = new Date();
        String strDate = sdfDate.format(now);
        return strDate;
    }

    public static Date GetCurrentDate() {

        return new Date();
    }

    public static String GetFormatDate(String fromDate, Locale locale) {
        try {
            DateFormat fromFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", ENGLISH);
            fromFormat.setLenient(false);

            DateFormat toFormat = new SimpleDateFormat("yyyy-MM-dd", locale);
            toFormat.setLenient(false);

            Date date = fromFormat.parse(fromDate);
            return toFormat.format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fromDate;
    }
    //    com.github.msarhan.ummalqura.calendar.UmmalquraCalendar
//    public static String GetFormattedHijriDate(String georgianDate, Locale locale, boolean withDayOfWeek) throws ParseException {
//        SimpleDateFormat hijriFormat = new SimpleDateFormat("", locale);
//        Calendar hijjriCalendar = new UmmalquraCalendar();
//        Calendar dateFromCale = Calendar.getInstance();
//        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", locale);
//        dateFromCale.setTime(sdf.parse(georgianDate));
//        hijjriCalendar.setTime(dateFromCale.getTime());
//        // hijriFormat.applyPattern("EEEE d MMMM, y");
//        if (withDayOfWeek)
//            return hijjriCalendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, locale) + "  ," + hijjriCalendar.get(Calendar.DAY_OF_MONTH) + "  " + hijjriCalendar.getDisplayName(Calendar.MONTH, Calendar.LONG, locale) + "  " + hijjriCalendar.get(Calendar.YEAR);
//        else
//            return hijjriCalendar.get(Calendar.DAY_OF_MONTH) + "  " + hijjriCalendar.getDisplayName(Calendar.MONTH, Calendar.LONG, locale) + "  " + hijjriCalendar.get(Calendar.YEAR);
//    }

}
