package fawzy.com.weathernavigator.dagger.module;

import android.app.Application;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {


    @Provides
    @Named("app")
    public Application provideApplication(@Named("app") Application application) {
        return application;
    }
}