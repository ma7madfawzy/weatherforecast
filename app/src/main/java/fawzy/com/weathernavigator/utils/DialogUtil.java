package fawzy.com.weathernavigator.utils;

import android.content.Context;
import android.graphics.Color;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class DialogUtil {
    static DialogUtil dialogUtil;

    public static DialogUtil getInstance() {
        if (dialogUtil == null)
            dialogUtil = new DialogUtil();
        return dialogUtil;
    }

    public void showDialogMsg(Context context, String title, String content, SweetAlertDialog.OnSweetClickListener listener) {
        SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#ffffff"));
        if (listener != null) pDialog.setConfirmButton("OK", null);
        pDialog.setTitleText(title);
        pDialog.setContentText(content);
        pDialog.setCancelable(true);
        pDialog.setCanceledOnTouchOutside(true);
        pDialog.setConfirmClickListener(listener);
        pDialog.show();
    }

    public void showDialogError(Context context, String title, String content, SweetAlertDialog.OnSweetClickListener listener) {
        SweetAlertDialog dialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
        dialog.getProgressHelper().setBarColor(Color.parseColor("#ffffff"));
        if (listener != null) dialog.setConfirmButton("ok", null);
        dialog.setTitle(title);
        dialog.setContentText(content);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setConfirmClickListener(listener);
        dialog.show();
    }

    public void showYesNoDialog(Context context, String title, String content, SweetAlertDialog.OnSweetClickListener cancellistener, SweetAlertDialog.OnSweetClickListener onClickListener) {
        SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#ffffff"));
        if (onClickListener != null) pDialog.setConfirmButton("Yes", null);
        if (cancellistener != null) pDialog.setCancelButton("No", null);
        pDialog.setTitleText(title);
        pDialog.setContentText(content);
        pDialog.setCancelable(true);
        pDialog.setConfirmClickListener(onClickListener);
        pDialog.setCancelClickListener(cancellistener);
        pDialog.setCanceledOnTouchOutside(true);
        pDialog.setCancelable(true);
        pDialog.show();
    }
}
