package fawzy.com.weathernavigator.utils;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import fawzy.com.weathernavigator.ui.base.BaseFragment;

public class FragmentMangerUtil {

    public static int subscreensOnTheStack = 0;
    private static FragmentMangerUtil fragmentMangerUtil;

    public static FragmentMangerUtil getInstance() {
        if (fragmentMangerUtil == null)
            fragmentMangerUtil = new FragmentMangerUtil();
        return fragmentMangerUtil;
    }

    public void replaceFragment(AppCompatActivity activity, Fragment fragment, int container) {
        Fragment theFragment = activity.getSupportFragmentManager().findFragmentByTag(fragment.getClass().getSimpleName());
        if (theFragment == null)
            activity.getSupportFragmentManager().beginTransaction()
                    .replace(container, fragment, fragment.getClass().getSimpleName())
                    .commit();
        else
            activity.getSupportFragmentManager().beginTransaction().show(theFragment).commit();
    }


    public void replaceFragment(AppCompatActivity activity, Fragment fragment, int container, Bundle bundle) {
        fragment.setArguments(bundle);
        replaceFragment(activity, fragment, container);
    }

    public void replaceFragment(FragmentManager fragmentManager, Fragment fragment, int container) {
        Fragment theFragment = fragmentManager.findFragmentByTag(fragment.getClass().getSimpleName());
        if (theFragment == null)
            fragmentManager.beginTransaction()
                    .replace(container, fragment, fragment.getClass().getSimpleName())
                    .commit();
        else
            fragmentManager.beginTransaction().show(theFragment).commit();
    }

    public void addSubFragment(FragmentManager fragmentManager, Fragment fragment, int container) {
        Fragment theFragment = fragmentManager.findFragmentByTag(fragment.getClass().getSimpleName());
        if (theFragment == null)
            fragmentManager
                    .beginTransaction()
                    .add(container, fragment, fragment.getClass().getSimpleName())
                    .addToBackStack(null)
                    .commit();
        else
            fragmentManager.beginTransaction().show(theFragment).commit();
        subscreensOnTheStack++;
    }


    public void replaceFragmentWithSave(FragmentActivity activity, Fragment fragment, int container) {
        Fragment theFragment = activity.getSupportFragmentManager().findFragmentByTag(fragment.getClass().getSimpleName());
        if (theFragment == null)
            activity.getSupportFragmentManager().beginTransaction()
                    .replace(container, fragment, fragment.getClass().getSimpleName())
                    .addToBackStack(fragment.getClass().getSimpleName())
                    .commit();
        else
            activity.getSupportFragmentManager().beginTransaction().show(theFragment).commit();
    }

    public void replaceFragmentWithSave(FragmentManager fragmentManager, Fragment fragment, int container) {
        Fragment theFragment = fragmentManager.findFragmentByTag(fragment.getClass().getSimpleName());
        if (theFragment == null)
            fragmentManager.beginTransaction()
                    .replace(container, fragment, fragment.getClass().getSimpleName())
                    .addToBackStack(fragment.getClass().getSimpleName())
                    .commit();
        else
            fragmentManager.beginTransaction().show(theFragment).commit();
    }


    public boolean isShown(FragmentManager supportFragmentManager, BaseFragment homeFragment) {
        return supportFragmentManager.findFragmentByTag(homeFragment.getClass().getSimpleName()) != null
                && supportFragmentManager.findFragmentByTag(homeFragment.getClass().getSimpleName()).isVisible();
    }

    public void popOffSubscreens(FragmentManager supportFragmentManager) {
        while (subscreensOnTheStack > 0) {
            supportFragmentManager.popBackStackImmediate();
            subscreensOnTheStack--;
        }
    }

}
