package fawzy.com.weathernavigator.ui.home.map;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;

import java.util.List;

import fawzy.com.weathernavigator.R;
import fawzy.com.weathernavigator.utils.MapperUtil;


/**
 * Created by Fawzy on 1/25/19
 */
public class MapFragment extends SupportMapFragment implements OnMapReadyCallback, GoogleMap.OnMapClickListener {

    private GoogleMap googleMap;
    private Marker modelMarker;
    private GoogleMapUtils.OnMapTouchListener mListener;
    private GoogleMapUtils mapUtils;
    private View layout;
    private LatLngBounds bounds;
    private boolean restrictMap;
    private boolean bundleMarker;
    private List<LatLng> latLngList;

    public static MapFragment getInstance(double longtude, double lattude, boolean... clickable) {
        MapFragment fragment = new MapFragment();
        Bundle bundle = new Bundle();
        bundle.putDouble("long", longtude);
        bundle.putDouble("lat", lattude);
        if (clickable.length > 0)
            bundle.putBoolean("clickable", clickable[0]);
        fragment.setArguments(bundle);
        return fragment;
    }

    public static MapFragment getInstance(String latLngs, boolean bounded) {
        MapFragment fragment = new MapFragment();
        Bundle bundle = new Bundle();
        bundle.putString("latLngList", latLngs);
        bundle.putBoolean("bounded", bounded);
        fragment.setArguments(bundle);
        return fragment;
    }

    public static MapFragment getInstance(boolean... clickable) {
        MapFragment fragment = new MapFragment();
        Bundle bundle = new Bundle();
        if (clickable.length > 0)
            bundle.putBoolean("clickable", clickable[0]);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getMapAsync(this::onMapReady);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        mapUtils = new GoogleMapUtils(googleMap, getContext());
        mapUtils.dispatchScrollview(layout, mListener);
        googleMap.setOnMapClickListener(this::onMapClick);
        checkArguments();
    }

    private void checkArguments() {
        if (getArguments() != null && getArguments().containsKey("long")) {
            modelMarker = mapUtils.setLocationWithAnimate_Zoom(getArgumentLocation(), 10);
            bundleMarker = true;
        } else if (getArguments() != null && getArguments().containsKey("latLngList")) {
            latLngList = MapperUtil.getLatLngs(getArguments().getString("latLngList"));
            highlightBounds(latLngList, true, true);
        }
    }

    /**
     * @Param restrictMap -> defines weather picking outside the bounds allowed or not(true -> not allowed)
     **/
    public void highlightBounds(List<LatLng> lstLatLngRoute, boolean restrictMap, boolean clearMap) {
        if (googleMap == null || lstLatLngRoute == null || lstLatLngRoute.isEmpty()) return;
        this.restrictMap = restrictMap;
        latLngList = lstLatLngRoute;
        if (!bundleMarker && clearMap) {
            googleMap.clear();
            modelMarker = null;
        } else bundleMarker = false;
        mapUtils.setMaxMinZoom(GoogleMapUtils.ZOOM_BETWEEN_CITY_AND_STREETS, GoogleMapUtils.ZOOM_TO_Buildings);
        bounds = mapUtils.highlightArea(latLngList, ContextCompat.getColor(getContext(), R.color.colorPrimary),
                1, restrictMap);
    }

    private Double getLatitude() {
        return getArguments().getDouble("lat");
    }


    private Double getLongitude() {
        return getArguments().getDouble("long");
    }

    private LatLng getArgumentLocation() {
        return new LatLng(getLatitude(), getLongitude());
    }

    @Override
    public void onMapClick(LatLng latLng) {
        modelMarker = mapUtils.updateMarker(latLng, modelMarker);
        updateHanlder(latLng);
    }

    private void updateHanlder(LatLng latLng) {
        try {
            OnPickLocation mapClickHandler = (OnPickLocation) getActivity();
            if (mapClickHandler != null)
                mapClickHandler.onLocationPicked(latLng);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle savedInstance) {
        layout = super.onCreateView(layoutInflater, viewGroup, savedInstance);
        return layout;
    }

    public void setListener(GoogleMapUtils.OnMapTouchListener listener) {
        mListener = listener;
    }

    public interface OnPickLocation {
        void onLocationPicked(LatLng location);
    }
}