package fawzy.com.weathernavigator.ui.base;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.WindowManager;

import fawzy.com.weathernavigator.R;
import fawzy.com.weathernavigator.constants.Constants;
import fawzy.com.weathernavigator.utils.LocalityUtil;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;

import static fawzy.com.weathernavigator.constants.Constants.isArabic;

/**
 * Created by fawzy on 1/25/19.
 */

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return false;
    }

    protected void setLocality(SharedPreferences preferences) {
        String lang = preferences.getString(Constants.Preferences_LANGUAGE, "ar");
        LocalityUtil.setLocality(this, lang);
        Log.e("TagLog: " + getClass().getSimpleName(), "lang= " + lang);
        isArabic = lang.equals("ar");
    }

    protected void setUpToolbar(Toolbar toolbar, int title, boolean displayHomeAsUp, int... indicatorResId) {
        toolbar.setTitle(title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(displayHomeAsUp);
        getSupportActionBar().setHomeAsUpIndicator(indicatorResId.length > 0 ? indicatorResId[0] :
                isArabic ? R.drawable.ic_arrow_right : R.drawable.ic_arrow_left);
    }

    protected void setUpToolbar(Toolbar toolbar, String title, boolean displayHomeAsUp, int... indicatorResId) {
        toolbar.setTitle(title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(displayHomeAsUp);
        getSupportActionBar().setHomeAsUpIndicator(indicatorResId.length > 0 ? indicatorResId[0] :
                isArabic ? R.drawable.ic_arrow_right : R.drawable.ic_arrow_left);
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

    protected abstract void renderView();


}
