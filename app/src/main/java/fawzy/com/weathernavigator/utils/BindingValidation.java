package fawzy.com.weathernavigator.utils;

/**
 * Created by fawzy on 1/25/19.
 */

import android.animation.ObjectAnimator;
import android.content.Context;
import android.databinding.BindingAdapter;
import android.support.annotation.IntDef;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.BounceInterpolator;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import de.hdodenhof.circleimageview.CircleImageView;


public class BindingValidation {
    private ValidationCallback callback;

    @BindingAdapter({"bind:setError", "bind:setType"})
    public static void setError(View view, String errorMsg, int type) {
        ErrorValidation errorValidation = new ErrorValidation();
        errorValidation.errorText = errorMsg;
        errorValidation.viewType = type;
        view.setTag(errorValidation);
    }

    @BindingAdapter({"bind:setError"})
    public static void setError(TextInputLayout view, int id) {
        ErrorValidation errorValidation = new ErrorValidation();
        errorValidation.errorText = view.getContext().getString(id);
        view.setTag(errorValidation);
    }

    @BindingAdapter({"bind:setError", "bind:setType", "bind:isIgnore"})
    public static void setError(View view, int id, int type, boolean isIgnore) {
        ErrorValidation errorValidation = new ErrorValidation();
        errorValidation.errorText = view.getContext().getString(id);
        errorValidation.viewType = type;
        errorValidation.isIgnore = isIgnore;
        view.setTag(errorValidation);
    }

    //  bind:setError="@{R.string.please_enter_complaint_number}"
    // bind:setType="@{TYPE.EDITTEXT}"
    @BindingAdapter({"bind:setValidationLength"})
    public static void setError(View view, int lenght) {
        ErrorValidation errorValidation = new ErrorValidation();
        errorValidation.errorText = 9 + Integer.toString(lenght);
        errorValidation.viewType = TYPE.EDITTEXT;
        errorValidation.length = lenght;
        view.setTag(errorValidation);
    }

    // bind:isIgnore="@{true}"
    @BindingAdapter({"bind:setError", "bind:setType", "bind:isIgnore", "bind:module"})
    public static void setError(final EditText view, int id, int type, boolean isIgnore, final BindingValidation validation_module) {
        final ErrorValidation errorValidation = new ErrorValidation();
        errorValidation.errorText = view.getContext().getString(id);
        errorValidation.viewType = type;
        errorValidation.isIgnore = isIgnore;
        view.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(final View v, boolean hasFocus) {
                if (!hasFocus) {
                    view.setError(errorValidation.errorText);
                    validation_module.validate(view.getContext(), view, validation_module.callback, false);
                }
            }
        });
        view.setTag(errorValidation);
    }

    public boolean validate(final Context context, final View v, ValidationCallback callback, boolean validateAllChilds) {
        boolean valid = true;
        if (v.getVisibility() == View.VISIBLE) {
            try {
                if (checkIfIgnore(v)) return true;
                if (v instanceof ViewGroup) {
                    ViewGroup vg = (ViewGroup) v;
                    for (int i = 0; i < vg.getChildCount(); i++) {
                        View child = vg.getChildAt(i);
                        if (v.getVisibility() == View.VISIBLE) {
                            if (!propgateError(v, callback)) {
                                valid = false;
                            }
                            if (!validate(context, child, callback, validateAllChilds)) {
                                valid = false;
                            }
                            if (!valid && !validateAllChilds)
                                return valid;
                        }
                    }
                } else {
                    if (v.getVisibility() == View.VISIBLE) {
                        if (!propgateError(v, callback)) {
                            valid = false;
                            if (!valid && !validateAllChilds)
                                return valid;
                        }

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return valid;
    }

    private boolean checkIfIgnore(View v) {
        ErrorValidation validation = (ErrorValidation) v.getTag();
        if (validation != null) {
            return validation.isIgnore;
        }
        return false;
    }

    public boolean propgateError(View view, ValidationCallback callback) {
        if (view.getTag() instanceof ErrorValidation) {
            ErrorValidation errorValidation = (ErrorValidation) view.getTag();
            String errorText = errorValidation.errorText;
            boolean isValid = true;
            switch (errorValidation.viewType) {
                case TYPE.EDITTEXT:
                    isValid = Validation_Utils.isValidData(((EditText) view).getText().toString());
                    int txtLength = ((EditText) view).getText().toString().length();
                    if (errorValidation.length != txtLength && errorValidation.length != -1) {
                        isValid = false;
                    }
                    if (!isValid) {
//                        view.requestFocus();
                        Validation_Utils.focus((EditText) view);
                        KeyboardUtil.getInstance().hideKeyboard(view);
                    }
//                    Validation_Utils.focus((EditText) view);
                    break;
                case TYPE.EMAIL:

                    isValid = ((EditText) view).getText().toString().trim().equals("") || Validation_Utils.isValidEmail(((EditText) view).getText().toString());
                    if (!isValid) {
//                        view.requestFocus();
                        Validation_Utils.focus((EditText) view);
                    }
//                    Validation_Utils.focus((EditText) view);
                    break;
                case TYPE.PHONE:
                    isValid = Validation_Utils.isValidSadiPhone(((EditText) view).getText().toString());
                    if (!isValid) {
//                        view.requestFocus();
                        Validation_Utils.focus((EditText) view);
                    }
//                    Validation_Utils.focus((EditText) view);
                    break;
                case TYPE.USERNAME:
                    isValid = Validation_Utils.isValidUsername(((EditText) view).getText().toString());
                    if (!isValid) {
//                        view.requestFocus();
                        Validation_Utils.focus((EditText) view);
                    }
                    break;
                case TYPE.USERNAMEWITHCHAR:
                    isValid = Validation_Utils.isValidUserName(((EditText) view).getText().toString());
                    if (!isValid) {
//                        view.requestFocus();
                        Validation_Utils.focus((EditText) view);
                    }
                    break;
                case TYPE.CHECKBOX:
                    isValid = ((CheckBox) view).isChecked();
                    if (!isValid) {
                        view.requestFocus();
                    }
                    break;
                case TYPE.SPINNER:
                    isValid = ((Spinner) view).getSelectedItemPosition() > 0;
                    if (!isValid) {
                        view.setFocusable(true);
                        view.requestFocusFromTouch();
                        view.requestFocus();
                        ((Spinner) view).performClick();
                    }

                    break;
                case TYPE.RADIOGROUP:
                    isValid = ((RadioGroup) view).getCheckedRadioButtonId() != 0;
                    if (!isValid) {
                        view.requestFocus();
                    }
                    break;
                case TYPE.IMAGEVIEW:
                    isValid = errorValidation.isValid;
                    if (!isValid) {
                        if (view instanceof CircleImageView) {
                            ((CircleImageView) view).setBorderColor(Utils.getColor(view.getContext(), android.R.color.holo_red_dark));
                            ((CircleImageView) view).setBorderWidth(3);
                            ObjectAnimator anim = ObjectAnimator.ofInt(((CircleImageView) view), "BorderWidth", 7, 0);
                            anim.setDuration(2000);                  // Duration in milliseconds
                            anim.setInterpolator(new BounceInterpolator());  // E.g. Linear, Accelerate, Decelerate
                            anim.setRepeatCount(3);
                            anim.start();
                        }
                        view.setFocusable(true);
                        view.requestFocusFromTouch();
                        view.requestFocus();
//                        view.performClick();
                    }
                    break;
            }
            if (!isValid) {
                callback.showCallbackAs(view, errorValidation.viewType, errorText);
                showError(view, errorText);
                return false;
            }
        }
        return true;
    }

    private void showError(View view, String errorText) {
        try {
            EditText editText = (EditText) view;
            editText.setError(errorText);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setCallback(ValidationCallback callback) {
        this.callback = callback;
    }


    public static interface ValidationCallback {
        public void showCallbackAs(View view, int viewType, String error);
    }

    @IntDef({TYPE.EDITTEXT, TYPE.SPINNER, TYPE.RADIOGROUP, TYPE.CHECKBOX})
    @Retention(RetentionPolicy.SOURCE)
    public static @interface TYPE {
        public int EDITTEXT = 1;
        public int SPINNER = 2;
        public int RADIOGROUP = 3;
        public int CHECKBOX = 4;
        public int IMAGEVIEW = 5;
        public int EMAIL = 6;
        public int PHONE = 7;
        public int USERNAME = 8;
        public int USERNAMEWITHCHAR = 9;

    }

    public static class ErrorValidation {
        @TYPE
        public int viewType;
        public String errorText;
        public boolean isValid = false;
        public boolean isIgnore = false;
        public int length = -1;

    }
}
