package fawzy.com.weathernavigator.dagger.module;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import fawzy.com.weathernavigator.AppClass;
import fawzy.com.weathernavigator.constants.Constants;
import fawzy.com.weathernavigator.data.Interceptors.CheckInternetInterceptor;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;

@Module(includes = {ContextModule.class})
public class OkHttpClientModule {

    @Provides
    @Singleton
    public OkHttpClient okHttpClient(@Named("cache") Cache cache, @Named("interceptor") Interceptor headerInterceptor, HttpLoggingInterceptor httpLoggingInterceptor) {
        return new OkHttpClient()
                .newBuilder()
                .cache(cache)
                .addInterceptor(headerInterceptor)
                .addInterceptor(httpLoggingInterceptor)
                .connectTimeout(Constants.REQUEST_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(Constants.REQUEST_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(Constants.REQUEST_TIMEOUT, TimeUnit.SECONDS)
                .build();
    }

    @Provides
    @Singleton
    @Named("cache")
    public Cache cache(@Named("cacheFile") File cacheFile) {
        return new Cache(cacheFile, 10 * 1000 * 1000); //10 MB
    }

    @Provides
    @Singleton
    @Named("cacheFile")
    public File file(@Named("context") Context context) {
        File file = new File(context.getCacheDir(), "MedictaHttpCache");
        file.mkdirs();
        return file;
    }

    @Provides
    @Singleton
    public HttpLoggingInterceptor httpLoggingInterceptor() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                Log.i("HttpLog", message);
            }
        });
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return httpLoggingInterceptor;
    }

    @Provides
    @Singleton
    CheckInternetInterceptor provideCheckInternetInterceptor() {
        return new CheckInternetInterceptor();
    }

    @Provides
    @Singleton
    @Named("interceptor")
    public Interceptor provideInterceptor() {
        return new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                if (!AppClass.getInstance().checkIfHasNetwork()) {
                    return chain.proceed(chain.request());
                }
                Request request = chain.request();
                Request newRequest;
                newRequest = request.newBuilder()
                        .addHeader("Content-Type", "application/json")
                        .addHeader("Accept-Language", Constants.isArabic ? "ar-eg" : "en-us")
                        .build();
                return chain.proceed(newRequest);
            }
        };
    }

}
