package fawzy.com.weathernavigator.data.Interceptors;


import java.io.IOException;

import fawzy.com.weathernavigator.AppClass;
import fawzy.com.weathernavigator.constants.Constants;
import okhttp3.Interceptor;
import okhttp3.Protocol;
import okhttp3.Response;
import okhttp3.ResponseBody;

/**
 * Created by Fawzy on 1/25/19.
 */

public class CheckInternetInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        if (AppClass.getInstance().checkIfHasNetwork()) {
            return chain.proceed(chain.request());
        }
        return new Response.Builder()
                .request(chain.request())
                .code(Constants.NO_INTERNET)
                .protocol(Protocol.HTTP_1_1)
                .body(ResponseBody.create(null, ""))
                .message("No Internet")
                .build();
    }
}
