package fawzy.com.weathernavigator.ui.home.main;

import android.databinding.Bindable;

import com.google.android.gms.maps.model.LatLng;

import fawzy.com.weathernavigator.BR;
import fawzy.com.weathernavigator.ui.base.BaseModel;

/**
 * Created by fawzy on 1/25/19.
 */

public class HomeModel extends BaseModel {
    private LatLng pickedLatLng;
    private boolean mapShown;

    public LatLng getPickedLatLng() {
        return pickedLatLng;
    }

    public void setPickedLatLng(LatLng pickedLatLng) {
        this.pickedLatLng = pickedLatLng;
    }

    @Bindable
    public boolean isMapShown() {
        return mapShown;
    }

    public void setMapShown(boolean mapShown) {
        this.mapShown = mapShown;
        notifyPropertyChanged(BR.mapShown);
    }
}

