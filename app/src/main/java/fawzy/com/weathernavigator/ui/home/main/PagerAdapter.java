package fawzy.com.weathernavigator.ui.home.main;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import fawzy.com.weathernavigator.R;
import fawzy.com.weathernavigator.ui.home.bookmarks.BookmarksFragment;
import fawzy.com.weathernavigator.ui.home.map.MapFragment;

/**
 * Created by fawzy on 1/25/19.
 */
public class PagerAdapter extends FragmentPagerAdapter {
    public static int mapIndex = 0;
    public static int bookmarkIndex = 1;
    private final LatLng pickedLatLng;
    String tabTitles[];
    private Context mContext;

    public PagerAdapter(Context context, FragmentManager fm, LatLng pickedLatLng) {
        super(fm);
        mContext = context;
        this.pickedLatLng = pickedLatLng;
        tabTitles = context.getResources().getStringArray(R.array.titles);
    }

    // This determines the fragment for each tab
    @Override
    public Fragment getItem(int position) {
        if (position == PagerAdapter.mapIndex) {
            return MapFragment.getInstance(pickedLatLng.longitude, pickedLatLng.latitude, true);
        } else if (position == PagerAdapter.bookmarkIndex) {
            return new BookmarksFragment();
        }
        return new BookmarksFragment();
    }

    // This determines the number of tabs
    @Override
    public int getCount() {
        return tabTitles.length;
    }

    // This determines the title for each tab
    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }

    public View getTabView(int position) {
        View tab = LayoutInflater.from(mContext).inflate(R.layout.custom_tab, null);
        TextView tv = (TextView) tab.findViewById(R.id.custom_text);
        tv.setText(tabTitles[position]);
        return tab;
    }

}
