package fawzy.com.weathernavigator.utils;

import android.support.design.widget.Snackbar;
import android.view.View;

public class SnackUtil {
    public static void showSnackBar(View view, int msg) {
        Snackbar.make(view, msg, Snackbar.LENGTH_SHORT).show();
    }

    public static void showSnackBar(View view, String msg) {
        Snackbar.make(view, msg, Snackbar.LENGTH_LONG).show();
    }
}
