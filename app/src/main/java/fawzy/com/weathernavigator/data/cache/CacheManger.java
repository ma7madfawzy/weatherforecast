package fawzy.com.weathernavigator.data.cache;

import java.util.ArrayList;
import java.util.List;

import fawzy.com.weathernavigator.constants.Constants;
import fawzy.com.weathernavigator.data.Resource;
import fawzy.com.weathernavigator.data.data_models.WeatherDM;
import io.paperdb.Paper;

import static fawzy.com.weathernavigator.constants.Constants.BOOKMARKS;

public class CacheManger {
    private static CacheManger cacheManger;

    public static CacheManger getInstance() {
        if (cacheManger == null)
            cacheManger = new CacheManger();
        return cacheManger;
    }

    public void cacheData(Resource<List<WeatherDM>> listResource) {
        Paper.book().write(Constants.BOOKMARKS, listResource);
    }

    public Resource<List<WeatherDM>> getCachedData() {
        return Paper.book().read(BOOKMARKS, Resource.empty(new ArrayList()));
    }
}
