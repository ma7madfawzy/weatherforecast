package fawzy.com.weathernavigator.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;

import java.util.Locale;

import fawzy.com.weathernavigator.constants.Constants;
import fawzy.com.weathernavigator.ui.home.main.HomeActivity;

public class LocalityUtil {
    public static void changeLanguage(Activity activity, SharedPreferences preferences, String lang) {
        preferences.edit().putString(Constants.Preferences_LANGUAGE, lang).apply();
        restart(activity);
    }

    public static void setLocality(Activity activity, String lang) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        activity.getBaseContext().getResources().updateConfiguration(config,
                activity.getBaseContext().getResources().getDisplayMetrics());
    }

    public static void setLocality(Context activity, String lang) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;

    }

    private static void restart(Activity activity) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            activity.finish();
            HomeActivity.start(activity);
        } else {
            activity.recreate();
        }
    }

}
