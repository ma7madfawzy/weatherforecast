package fawzy.com.weathernavigator.data.Interceptors;

import android.annotation.SuppressLint;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Fawzy on 1/25/19.
 */

public class HeaderInterceptor implements Interceptor {
    @SuppressLint("LongLogTag")
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request.Builder request = chain.request().newBuilder();
//TODO add headers if required
        return chain.proceed(request.build());
    }
}
