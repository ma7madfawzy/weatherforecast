package fawzy.com.weathernavigator.ui.home.main;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import fawzy.com.weathernavigator.constants.Constants;
import fawzy.com.weathernavigator.data.Resource;
import fawzy.com.weathernavigator.data.Status;
import fawzy.com.weathernavigator.data.data_models.WeatherDM;
import fawzy.com.weathernavigator.data.services.UserService;


/**
 * Created by fawzy on 7/23/18.
 */

public class HomeVM extends ViewModel {
    private MutableLiveData<Resource<WeatherDM>> data = new MutableLiveData<>();
    private MutableLiveData<Resource<List<WeatherDM>>> dataList = new MutableLiveData<>();
    private UserService iServices;
    private HomeModel model;
    private HomeRepo homeRepo;
    private HomeActivity activity;

    public HomeVM() {

    }

    public MutableLiveData<Resource<WeatherDM>> getWeatherData(LatLng latLng) {
        model.enableLoading();
        homeRepo.sendRequest(latLng.latitude, latLng.longitude, Constants.WaetherMapKEY).observe(activity, data -> this.data.setValue(data));
        return data;

    }

    void set(HomeActivity activity, UserService iServices, HomeModel model, Resource<List<WeatherDM>> cachedData) {
        this.iServices = iServices;
        this.model = model;
        this.homeRepo = new HomeRepo(iServices);
        this.activity = activity;
        observeData();
        dataList.setValue(cachedData);
    }

    private void observeData() {
        data.observe(activity, weatherDMResource -> {
            model.setResource(weatherDMResource);
            if (weatherDMResource.getStatus() == Status.SUCCESS) {
                onNewItemAdded(weatherDMResource);
            }
        });
    }

    private void onNewItemAdded(Resource<WeatherDM> weatherDMResource) {
        ((HomeHandler) activity).onItemAdded();
        Resource<List<WeatherDM>> newValue = dataList.getValue();
        List<WeatherDM> newData = newValue.getData();
        newData.add(weatherDMResource.getData());
        newValue.setData(newData);
        newValue.setStatus(weatherDMResource.getStatus());
        dataList.setValue(newValue);
    }

    public MutableLiveData<Resource<List<WeatherDM>>> getDataList() {
        return dataList;
    }

    public void deleteItem(int position) {
        Resource<List<WeatherDM>> newValue = dataList.getValue();
        List<WeatherDM> newData = newValue.getData();
        newData.remove(position);
        newValue.setData(newData);
        newValue.setStatus(newData.size() == 0 ? Status.EMPTY_DATA : newValue.getStatus());
        dataList.setValue(newValue);
    }
}
