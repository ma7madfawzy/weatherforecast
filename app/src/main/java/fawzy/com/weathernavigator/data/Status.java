package fawzy.com.weathernavigator.data;

/**
 * Created by Fawzy on  1/25/19.
 */

public enum Status {SUCCESS, ERROR, LOADING, EMPTY_DATA}
