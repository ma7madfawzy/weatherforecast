package fawzy.com.weathernavigator;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.multidex.MultiDexApplication;

import javax.inject.Inject;

import fawzy.com.weathernavigator.constants.Constants;
import fawzy.com.weathernavigator.constants.ServicesConstants;
import fawzy.com.weathernavigator.dagger.component.AppComponent;
import fawzy.com.weathernavigator.dagger.component.DaggerAppComponent;
import fawzy.com.weathernavigator.utils.LocalityUtil;
import io.github.inflationx.calligraphy3.CalligraphyConfig;
import io.github.inflationx.calligraphy3.CalligraphyInterceptor;
import io.github.inflationx.viewpump.ViewPump;
import io.paperdb.Paper;


/**
 * Created by fawzy on 1/25/19.
 */

public class AppClass extends MultiDexApplication {
    private static AppClass instance;
    @Inject
    SharedPreferences preferences;
    private AppComponent appComponent;

    public static AppClass getInstance() {
        return instance;
    }

    public static String getServerErrorString() {
        return getInstance().getString(R.string.couldntReachTheServer);
    }

    public static String getStringValue(int resId) {
        return getInstance().getString(resId);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        setFont();
        setAppComponent();
        setLocality();
        setAppLanguageValue();
        Paper.init(this);
    }

    private void setLocality() {
        String lang = preferences.getString(Constants.Preferences_LANGUAGE, "ar");
        LocalityUtil.setLocality(this, lang);
        Constants.isArabic = lang.equals("ar");
    }

    private void setAppComponent() {
        appComponent = DaggerAppComponent.builder()
                .application(this)
                .context(this.getApplicationContext())
                .baseURL(ServicesConstants.BASE_URL)
                .build();
        appComponent.inject(this);
    }

    protected void setAppLanguageValue() {
        Constants.isArabic = preferences.getString(Constants.Preferences_LANGUAGE, "ar").equals("ar") ? true : false;
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    public boolean checkIfHasNetwork() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    private void setFont() {
        ViewPump.init(ViewPump.builder()
                .addInterceptor(new CalligraphyInterceptor(
                        new CalligraphyConfig.Builder()
                                .setDefaultFontPath(Constants.FONT_PATH)
                                .setFontAttrId(R.attr.fontPath)
                                .build())).build());
    }

}
