package fawzy.com.weathernavigator.utils;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;


public class MapperUtil {
    /**
     * maps a list of type F to list of type T"
     **/


    public static List<LatLng> getLatLngs(String string) {
        Type listType = new TypeToken<List<LatLng>>() {
        }.getType();
        List<LatLng> latLngs = new Gson().fromJson(string, listType);
        return latLngs;
    }
}