package fawzy.com.weathernavigator.dagger.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import fawzy.com.weathernavigator.data.services.UserService;
import retrofit2.Retrofit;

@Module(includes = {RetrofitModule.class})
public class IServicesModule {
    @Singleton
    @Provides
    UserService provideService(Retrofit retrofit) {
        return retrofit.create(UserService.class);
    }
}
