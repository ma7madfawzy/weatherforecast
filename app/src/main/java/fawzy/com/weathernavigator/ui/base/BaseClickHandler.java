package fawzy.com.weathernavigator.ui.base;

import android.view.View;

public interface BaseClickHandler {
    void onRowClicked(View v, int position);
}
