package fawzy.com.weathernavigator.data.services;

import fawzy.com.weathernavigator.data.data_models.WeatherDM;
import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by fawzy on 1/25/19.
 */

public interface UserService {
    @GET("weather")
    Single<Response<WeatherDM>> getWeatherData(@Query("lat") Double lat, @Query("lon") Double lng, @Query("APPID") String APPID);
}
