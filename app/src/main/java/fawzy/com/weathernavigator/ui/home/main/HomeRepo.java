package fawzy.com.weathernavigator.ui.home.main;

import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import fawzy.com.weathernavigator.AppClass;
import fawzy.com.weathernavigator.data.Resource;
import fawzy.com.weathernavigator.data.data_models.WeatherDM;
import fawzy.com.weathernavigator.data.services.UserService;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by fawzy 1/25/19.
 */
public class HomeRepo {
    private UserService iServices;

    public HomeRepo(UserService iServices) {
        this.iServices = iServices;
    }

    MutableLiveData<Resource<WeatherDM>> sendRequest(double lat, double lng, String apiId) {
        MutableLiveData<Resource<WeatherDM>> data = new MutableLiveData<>();
        data.setValue(Resource.loading(null));
        iServices.getWeatherData(lat, lng, apiId).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(value -> data.setValue(Resource.success(value.body())), e -> {
                    data.setValue(Resource.error(AppClass.getServerErrorString(), null));
                    Log.e("TagLog: " + getClass().getSimpleName(), e.getLocalizedMessage());
                });

        return data;
    }

}
