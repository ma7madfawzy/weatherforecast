package fawzy.com.weathernavigator.data.data_models.base;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import fawzy.com.weathernavigator.data.data_models.ResponseObject;

/**
 * Created by fawzy on 1/25/19.
 */

public class BaseResponse<T extends ResponseObject> {
    @SerializedName("results")
    @Expose
    private List<T> response;
    @SerializedName("errorCode")
    @Expose
    private int errorCore;
    @SerializedName("page")
    @Expose
    private int page;
    @SerializedName("total_results")
    @Expose
    private int total_results;
    @SerializedName("total_pages")
    @Expose
    private int total_pages;
    @SerializedName("status_message")
    @Expose
    private String status_message;

    public String getStatus_message() {
        return status_message;
    }

    public void setStatus_message(String status_message) {
        this.status_message = status_message;
    }


    public List<T> getResponse() {
        return response;
    }

    public BaseResponse setResponse(List<T> response) {
        this.response = response;
        return this;
    }

    public int getErrorCore() {
        return errorCore;
    }

    public void setErrorCore(int errorCore) {
        this.errorCore = errorCore;
    }

}
