package fawzy.com.weathernavigator.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import fawzy.com.weathernavigator.constants.ServicesConstants;


/**
 * Created by fawzy on 1/25/19.
 */

public class ImageLoader {
    private static ImageLoader imageLoader;

    public static ImageLoader getInstance() {
        if (imageLoader == null)
            imageLoader = new ImageLoader();
        return imageLoader;
    }

    public void loadImage(Context activity, ImageView imageView, String url) {
        if (url == null || url.isEmpty() || imageView == null)
            return;
        Glide.with(activity).load(ServicesConstants.IMAGE_BASE_URL + url)
                .into(imageView);
    }

    public void loadImageWithoutCache(Context activity, ImageView imageView, String url) {
        if (url == null || url.isEmpty() || imageView == null)
            return;

        Picasso.with(activity).load(ServicesConstants.IMAGE_BASE_URL + url).skipMemoryCache().memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE).into(imageView);
    }

    public void loadImageFromDrawable(Context activity, ImageView imageView, String resName) {
        imageView.setImageDrawable(getDrawable(activity, resName));
    }

    public Drawable getDrawable(Context context, String resName) {
        Resources resources = context.getResources();
        final int resourceId = resources.getIdentifier(resName, "drawable",
                context.getPackageName());
        return resources.getDrawable(resourceId);
    }
}
