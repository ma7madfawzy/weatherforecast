package fawzy.com.weathernavigator.utils;

/**
 * Created by fawzy on 1/25/19.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.BindingAdapter;
import android.databinding.BindingConversion;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.support.annotation.ColorRes;
import android.support.annotation.StringDef;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Locale;

import fawzy.com.weathernavigator.AppClass;
import fawzy.com.weathernavigator.R;
import fawzy.com.weathernavigator.ui.base.BaseActivity;
import kotlin.jvm.internal.Intrinsics;


public class BindingUtil {
    public static final int SELECTED = 2;
    public static final int UNSELECTED = 1;
    public static final int INDECATOR = 0;


    public BindingUtil(Context context) {
//        this.context = context;
//        if (Constants.isArabic) {
//            locality.set(LANGUAGE.ARABIC);
//            isArabic.set(true);
//            isEnglish.set(false);
//        } else {
//            locality.set(LANGUAGE.ENGLISH);
//            isArabic.set(false);
//            isEnglish.set(true);
//        }
    }

    @BindingAdapter({"bind:src"})
    public static void setImageBackground(ImageView view, Drawable drawable) {
        view.setImageDrawable(drawable);
    }

    @BindingAdapter({"bind:imageUrl"})
    public static void loadImage(ImageView view, String imageUrl) {
        ImageLoader.getInstance().loadImage(view.getContext(), view, imageUrl);
    }

    @BindingAdapter({"bind:imageUrlNoCash"})
    public static void loadImageWitoutCash(ImageView view, String imageUrl) {
        ImageLoader.getInstance().loadImageWithoutCache(view.getContext(), view, imageUrl);
    }

    @BindingAdapter("bind:drawableStart")
    public static void setDrawableStart(EditText et, final ImageView img) {
        et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() >= 4) {
                    img.setVisibility(View.VISIBLE);
                } else {
                    img.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @BindingAdapter("bind:appVersion")
    public static void setAppVersion(TextView textView, String versionStr) {
        try {
            textView.setText(versionStr + textView.getContext().getPackageManager().getPackageInfo(textView.getContext().getPackageName(), 0));
        } catch (PackageManager.NameNotFoundException e) {
            textView.setText(versionStr);
        }
    }

    @BindingAdapter("bind:setClickAction")
    public static void setDrawableStart(ImageView img, final String reference) {
        BaseActivity activity = (BaseActivity) img.getContext();
    }

    @BindingAdapter("bind:formatLongDate")
    public static void formatLongDate(TextView textView, String date) {
        try {
            textView.setText(DateFormatUtil.formatLongDate(Long.valueOf(date), "dd MMM yyyy - hh:mm"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static boolean isInteger(String str) {
        if (str == null) {
            return false;
        }
        int length = str.length();
        if (length == 0) {
            return false;
        }
        int i = 0;
        if (str.charAt(0) == '-') {
            if (length == 1) {
                return false;
            }
            i = 1;
        }
        for (; i < length; i++) {
            char c = str.charAt(i);
            if (c < '0' || c > '9') {
                return false;
            }
        }
        return true;
    }

    @BindingAdapter("bind:visible")
    public static void setVisibilty(final View v, boolean isVisible) {
        if (isVisible) {
            v.setVisibility(View.VISIBLE);
        } else {
            v.setVisibility(View.GONE);
        }
    }


    @BindingAdapter("bind:dateFormat")
    public static void setDate(TextView txt, String date) {
        Locale currentLocale = txt.getContext().getResources().getConfiguration().locale;
        txt.setText(DateFormatUtil.ConvertFromFormatUTC(currentLocale, date));
    }

    @BindingAdapter("setViewVisibilty")
    public static void hasData(View v, String ss) {
        if (ss != null && !ss.isEmpty()) {
            v.setVisibility(View.VISIBLE);
        } else {
            v.setVisibility(View.GONE);
        }
    }

    // TODO this method has two parameters, the first must be from type View (Noaman)
//    @BindingAdapter("bind:setBindingVisibility")
//    public static void hasData(ViewDataBinding v, boolean isVisible) {
//        if (isVisible) {
//            v.getRoot().setVisibility(View.VISIBLE);
//        } else {
//            v.getRoot().setVisibility(View.GONE);
//        }
//    }

    private static BroadcastReceiver getAfterDownloadComplete(final String dir, final Context mContext) {
        return new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                SnackbarUtil.showSnackBar(mContext, dir).show();
            }
        };
    }

    @BindingAdapter("bind:align")
    public static void align(View view, boolean arabic) {
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
        if (arabic) {
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        } else {
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        }
        view.setLayoutParams(layoutParams);
    }

    @BindingAdapter("bind:rotattoDirection")
    public static void alignusingRotation(View view, boolean arabic) {

        if (arabic) {
            view.setRotationY(180f);
        } else {
            view.setRotationY(0);
        }

    }

    @BindingAdapter("bind:direction")
    public static void direction(View view, boolean arabic) {
        if (arabic) {
            ViewCompat.setLayoutDirection(view, ViewCompat.LAYOUT_DIRECTION_RTL);
        } else {
            ViewCompat.setLayoutDirection(view, ViewCompat.LAYOUT_DIRECTION_LTR);
        }
    }

    @BindingAdapter("bind:visibility")
    public static void visibility(View view, boolean visible) {
        if (visible) {
            view.setVisibility(View.VISIBLE);
        } else {
            view.setVisibility(View.GONE);
        }
    }

    //bind:gravity="@{languageModel.isArabic}"
    @BindingAdapter("bind:gravity")
    public static void gravity(View view, boolean arabic) {
        if (view instanceof EditText) {
            if (arabic) {
//     if (((EditText) view).getInputType()== EditorInfo.TYPE_TEXT_VARIATION_PASSWORD)
                ((EditText) view).setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
            } else {
//                if (((EditText) view).getInputType()== EditorInfo.TYPE_TEXT_VARIATION_PASSWORD)
                ((EditText) view).setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
            }
        } else if (view instanceof TextView) {
            if (arabic) {
                ((TextView) view).setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
            } else {
                ((TextView) view).setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
            }
        } else if (view instanceof LinearLayout) {
            if (arabic) {
                ((LinearLayout) view).setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
            } else {
                ((LinearLayout) view).setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
            }
        } else if (view instanceof NavigationView) {
            DrawerLayout.LayoutParams params = (DrawerLayout.LayoutParams) ((NavigationView) view).getLayoutParams();
            if (arabic) {

                params.gravity = Gravity.RIGHT | Gravity.CENTER_VERTICAL;
            } else {
                params.gravity = Gravity.LEFT | Gravity.CENTER_VERTICAL;
            }
        } else if (view instanceof TextInputLayout) {
            if (arabic) {
                ((TextInputLayout) view).setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
            } else {
                ((TextInputLayout) view).setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
            }
        } else if (view instanceof Spinner) {
            if (arabic) {
                ((Spinner) view).setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
            } else {
                ((Spinner) view).setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
            }
        }
    }

    @BindingAdapter("bind:disabled")
    public static void disableView(View view, Boolean disabled) {
        view.setEnabled(!disabled);
        view.setAlpha(!disabled ? 1f : .5f);
    }

    @BindingAdapter("bind:drawableLeftDirection")
    public static void drawable(View view, Boolean arabic) {
        Drawable left = null;
        if (view instanceof TextView) {
            left = ((TextView) view).getCompoundDrawables()[0];
            if (arabic) {
                ((TextView) view).setCompoundDrawablesWithIntrinsicBounds(null, null, left, null);
            } else {
                ((TextView) view).setCompoundDrawablesWithIntrinsicBounds(left, null, null, null);
            }
        } else if (view instanceof Button) {
            left = ((Button) view).getCompoundDrawables()[2];
            if (arabic) {
                ((Button) view).setCompoundDrawablesWithIntrinsicBounds(null, null, left, null);
            } else {
                ((Button) view).setCompoundDrawablesWithIntrinsicBounds(left, null, null, null);
            }
        }

    }

    @BindingAdapter("bind:underline")
    public static void underline(View view, boolean doUnderline) {
        if (view instanceof TextView) {
            TextView textView = (TextView) view;
            if (doUnderline) {
                SpannableString content = new SpannableString(textView.getText().toString());
                content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
                textView.setText(content);
            }
        } else if (view instanceof CheckBox) {
            CheckBox checkBox = (CheckBox) view;
            if (doUnderline) {
                SpannableString content = new SpannableString(checkBox.getText().toString());
                content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
                checkBox.setText(content);
            }
        }
    }

    @BindingAdapter("bind:addAstress")
    public static void addAstress(View view, String text) {
        if (view instanceof TextView) {
            SpannableString str = new SpannableString("* " + text);
            str.setSpan(new ForegroundColorSpan(Color.RED),
                    0, 1,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            ((TextView) view).setText(str);
        }
    }

    // bind:addColorAstress="@{true}"
    @BindingAdapter("bind:addColorAstress")
    public static void addColorAstress(View view, boolean add) {
        if (add == false) return;
        if (view instanceof TextView) {
            TextView textView = (TextView) view;
//            SpannableUtils.SpanColoredTwoSectors(textView, "*", android.R.color.holo_red_dark, textView.getText().toString(), R.color.colorAccent);
        }
    }


    @BindingAdapter("bind:fontBold")
    public static void fontBold(View view, boolean isArabic) {
        if (view instanceof TextView) {
            TextView textView = (TextView) view;
            Typeface type = Typeface.createFromAsset(view.getContext().getAssets(), isArabic ? "DroidKufi-Bold.ttf" : "AvenirLTStd-Black.otf");
            textView.setTypeface(type);
        }
    }

    @BindingAdapter("setLineInCenter")
    public static void putLineCenterTextView(TextView view, Boolean isCenter) {
        view.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
    }

    @BindingAdapter("setRatingBarStarYellow")
    public static void setRatingBarStarYellow(ProgressBar ratingBar, @ColorRes int color) {
        ratingBar = new ProgressBar(AppClass.getInstance());
        LayerDrawable layerDrawable = (LayerDrawable) ratingBar.getProgressDrawable();
        layerDrawable.getDrawable(SELECTED).setColorFilter(ContextCompat.getColor(AppClass.getInstance(), color), PorterDuff.Mode.SRC_ATOP);
        layerDrawable.getDrawable(UNSELECTED).setColorFilter(ContextCompat.getColor(AppClass.getInstance(), R.color.lightGray), PorterDuff.Mode.SRC_ATOP);
        layerDrawable.getDrawable(INDECATOR).setColorFilter(ContextCompat.getColor(AppClass.getInstance(), R.color.lightGray), PorterDuff.Mode.SRC_ATOP);
    }

    @BindingConversion
    public static int convertBooleanToVisibility(boolean visible) {
        return visible ? View.VISIBLE : View.GONE;
    }


    @BindingAdapter({"setRoundBackGround"})
    public final void setRoundBackGround(@NotNull CheckBox receiver, float radius) {
        Intrinsics.checkParameterIsNotNull(receiver, "$receiver");
        SpannableStringBuilder stringBuilder = new SpannableStringBuilder();
        stringBuilder.append((CharSequence) (new StringBuilder()).append(" ").append(receiver.getText()).append(" ").toString());
        stringBuilder.setSpan(new RoundedBackgroundSpan(receiver.getContext(), radius), 0, stringBuilder.length(), 33);
        receiver.setText((CharSequence) stringBuilder);
    }

    @StringDef({LANGUAGE.ARABIC, LANGUAGE.ENGLISH})
    @Retention(RetentionPolicy.SOURCE)
    public @interface LANGUAGE {
        String ARABIC = "ar";
        String ENGLISH = "en";
    }


}
