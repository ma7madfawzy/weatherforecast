package fawzy.com.weathernavigator.data.data_models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by fawzy on 1/25/19.
 */

public class Main implements Parcelable {

    @SuppressWarnings("unused")
    public static final Creator<Main> CREATOR = new Creator<Main>() {
        @Override
        public Main createFromParcel(Parcel in) {
            return new Main(in);
        }

        @Override
        public Main[] newArray(int size) {
            return new Main[size];
        }
    };
    private float temp;
    private float pressure;
    private int humidity;
    private float temp_min;
    private float temp_max;

    public Main() {
    }

    private Main(Parcel in) {
        temp = in.readFloat();
        pressure = in.readFloat();
        humidity = in.readInt();
        temp_min = in.readFloat();
        temp_max = in.readFloat();
    }

    public float getTemp() {
        return (int) Math.round(temp - 273.15);
    }

    public void setTemp(float temp) {
        this.temp = temp;
    }

    public float getPressure() {
        return pressure;
    }

    public void setPressure(float pressure) {
        this.pressure = pressure;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public float getTemp_min() {
        return (int) Math.round(temp_min - 273.15);
    }

    public void setTemp_min(float temp_min) {
        this.temp_min = temp_min;
    }

    public float getTemp_max() {
        return (int) Math.round(temp_max - 273.15);
    }

    public void setTemp_max(float temp_max) {
        this.temp_max = temp_max;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeFloat(temp);
        dest.writeFloat(pressure);
        dest.writeInt(humidity);
        dest.writeFloat(temp_min);
        dest.writeFloat(temp_max);
    }


}
