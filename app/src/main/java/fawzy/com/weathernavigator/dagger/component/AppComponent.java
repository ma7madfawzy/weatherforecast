package fawzy.com.weathernavigator.dagger.component;

import android.app.Application;
import android.content.Context;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import fawzy.com.weathernavigator.AppClass;
import fawzy.com.weathernavigator.dagger.module.AppModule;
import fawzy.com.weathernavigator.dagger.module.ContextModule;
import fawzy.com.weathernavigator.dagger.module.IServicesModule;
import fawzy.com.weathernavigator.dagger.module.OkHttpClientModule;
import fawzy.com.weathernavigator.dagger.module.RetrofitModule;
import fawzy.com.weathernavigator.dagger.module.SharedModule;
import fawzy.com.weathernavigator.ui.home.bookmarks.BookmarksFragment;
import fawzy.com.weathernavigator.ui.home.bookmarks.weather_details.WeatherDetailsActivity;
import fawzy.com.weathernavigator.ui.home.main.HomeActivity;

@Singleton
@Component(modules = {SharedModule.class, ContextModule.class, AppModule.class, OkHttpClientModule.class, RetrofitModule.class, IServicesModule.class})
public interface AppComponent {
    void inject(BookmarksFragment bookmarksFragment);

    void inject(AppClass appClass);

    void inject(HomeActivity homeActivity);

    void inject(WeatherDetailsActivity weatherDetailsActivity);

    @Component.Builder
    interface Builder {
        @BindsInstance
        AppComponent.Builder application(@Named("app") Application application);

        @BindsInstance
        AppComponent.Builder context(@Named("context") Context context);

        @BindsInstance
        AppComponent.Builder baseURL(@Named("baseURL") String baseURL);

        AppComponent build();
    }
}

