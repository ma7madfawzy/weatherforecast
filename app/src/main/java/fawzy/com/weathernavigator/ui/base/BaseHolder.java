package fawzy.com.weathernavigator.ui.base;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by fawzy on 1/25/19.
 */

public class BaseHolder extends RecyclerView.ViewHolder {

    protected int lastAnimatedPosition;

    public BaseHolder(View itemView, int... lastVisiblePosition) {
        super(itemView);
//        RecyclerItemAnimator.runEnterAnimation(itemView, getAdapterPosition(), lastAnimatedPosition);
//        RecyclerItemAnimator.runEnterAnimation(itemView, getAdapterPosition(), lastVisiblePosition, lastAnimatedPosition);
        lastAnimatedPosition = getAdapterPosition();
    }

    public void bind(int position) {


    }
}
