package fawzy.com.weathernavigator.data.data_models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.view.View;

import fawzy.com.weathernavigator.BR;

/**
 * Created by fawzy on 1/25/19.
 */

public class ResponseObject extends BaseObservable {
    private String title;
    private String poster_path;

    @Bindable
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
        notifyPropertyChanged(BR.title);
    }

    @Bindable
    public String getPoster_path() {
        return poster_path;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
        notifyPropertyChanged(BR.poster_path);
    }

    public void onShowImageClicked(View view) {
//        ImageView imageView = (ImageView) view;
//        ImagePopup imagePopup = new ImagePopup(view.getContext());
//        imagePopup.initiatePopup(imageView.getDrawable());
//        imagePopup.viewPopup();
    }
}
