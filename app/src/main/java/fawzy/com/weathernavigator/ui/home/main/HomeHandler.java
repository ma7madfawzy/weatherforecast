package fawzy.com.weathernavigator.ui.home.main;

public interface HomeHandler {
    void deleteItem(int position);

    void openMap();

    void onItemAdded();
}
