package fawzy.com.weathernavigator.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

import fawzy.com.weathernavigator.R;
import fawzy.com.weathernavigator.ui.base.BaseActivity;

import static fawzy.com.weathernavigator.constants.Constants.FONT_PATH;


/**
 * Created by fawzy on 1/25/19.
 */

public class SnackbarUtil {
    public static void snackbar(Context context, @StringRes int id) {
        Snackbar.make(((Activity) context).findViewById(android.R.id.content), id, Snackbar.LENGTH_SHORT).show();
    }

    public static void snackbar(Activity activity, @StringRes int id) {
        Snackbar.make(activity.findViewById(android.R.id.content), id, Snackbar.LENGTH_SHORT).show();
    }

    public static void snackbar(Activity activity, String s) {
        Snackbar.make(activity.findViewById(android.R.id.content), s, Snackbar.LENGTH_SHORT).show();

    }

    public static Snackbar showSnackBar(Activity activity, @StringRes int id, View.OnClickListener action) {
        final Snackbar snackbar = Snackbar.make(activity.findViewById(android.R.id.content), id, Snackbar.LENGTH_SHORT);
        if (action != null) {
            snackbar.setAction("retry", action);
        } else {
            snackbar.setAction("OK", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    snackbar.dismiss();
                }
            });
        }
        snackbar.setActionTextColor(activity.getResources().getColor(R.color.colorAccent))
                .setDuration(8000);
        setSnackFont(snackbar, activity);
        return snackbar;

    }

    public static void setSnackFont(Snackbar snackbar, Context c) {
        TextView tv = (TextView) (snackbar.getView()).findViewById(android.support.design.R.id.snackbar_text);
        TextView snackbarActionTextView = (TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_action);
        Typeface font = Typeface.createFromAsset(c.getAssets(), FONT_PATH);
        tv.setTypeface(font);

        if (snackbarActionTextView.getText() != "") {
            snackbarActionTextView.setTypeface(font);
        } else {
            tv.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        }
    }

    public static Snackbar showSnackBar(Context context, String message) {
        Snackbar snackbar = Snackbar.make(((Activity) context).findViewById(android.R.id.content), message, Snackbar.LENGTH_SHORT);
        setSnackFont(snackbar, context);
        return snackbar;
    }

    public static Snackbar showSnackBar(Context context, int message) {
        Snackbar snackbar = Snackbar.make(((Activity) context).findViewById(android.R.id.content), message, Snackbar.LENGTH_SHORT);
        setSnackFont(snackbar, context);
        return snackbar;
    }

    public static Snackbar showSnackBarOriginal(Context context, int message) {
        return Snackbar.make(((BaseActivity) context).getWindow().getDecorView().getRootView(), message, Snackbar.LENGTH_SHORT);

    }

    public static Snackbar showSnackBar(Activity activity, String message, View.OnClickListener action) {
        final Snackbar snackbar = Snackbar.make(activity.findViewById(android.R.id.content), message, Snackbar.LENGTH_SHORT);
        setSnackFont(snackbar, activity);
        if (action != null) {
            snackbar.setAction("retry", action);
        } else {
            snackbar.setAction("OK", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    snackbar.dismiss();
                }
            });
        }
        snackbar.setActionTextColor(activity.getResources().getColor(R.color.colorAccent))
                .setDuration(8000);
        return snackbar;

    }


    public static Snackbar showSnackBar(Context context, @StringRes int id, View.OnClickListener action) {
        if (context == null || ((Activity) context).findViewById(android.R.id.content) == null)
            return showSnackBarOriginal(context, id);
        Snackbar s = Snackbar.make(((Activity) context).findViewById(android.R.id.content), id, Snackbar.LENGTH_SHORT)
                .setAction("retry", action)
                .setActionTextColor(context.getResources().getColor(android.R.color.holo_red_dark))
                .setDuration(8000);
        setSnackFont(s, context);
        return s;

    }
}
