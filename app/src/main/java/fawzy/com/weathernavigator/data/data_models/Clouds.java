package fawzy.com.weathernavigator.data.data_models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by fawzy on 1/25/19.
 */

public class Clouds implements Parcelable {

    @SuppressWarnings("unused")
    public static final Creator<Clouds> CREATOR = new Creator<Clouds>() {
        @Override
        public Clouds createFromParcel(Parcel in) {
            return new Clouds(in);
        }

        @Override
        public Clouds[] newArray(int size) {
            return new Clouds[size];
        }
    };
    private int all;

    public Clouds() {
    }

    private Clouds(Parcel in) {
        all = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(all);
    }

}
