package fawzy.com.weathernavigator.ui.home.bookmarks;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import fawzy.com.weathernavigator.R;
import fawzy.com.weathernavigator.constants.Constants;
import fawzy.com.weathernavigator.data.data_models.WeatherDM;
import fawzy.com.weathernavigator.databinding.BookmarkRowBinding;
import fawzy.com.weathernavigator.ui.base.BaseHolder;
import fawzy.com.weathernavigator.ui.home.bookmarks.weather_details.WeatherDetailsActivity;
import fawzy.com.weathernavigator.ui.home.main.HomeHandler;

/**
 * Created by fawzy on 1/25/19
 */
public class BookmarksAdapter extends RecyclerView.Adapter<BookmarksAdapter.ViewHolder> {

    private ArrayList<WeatherDM> dataList;
    private LayoutInflater layoutInflater;
    private int lastVisiblePosition;
    private HomeHandler homeHandler;
    private Context context;

    public BookmarksAdapter(List<WeatherDM> dataList, Context context) {
        super();
        this.dataList = new ArrayList<>(dataList);
        this.context = context;
        homeHandler = (HomeHandler) context;
    }

    public void removeAt(int position) {
        dataList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, dataList.size());
    }

    public ArrayList<WeatherDM> getDataList() {
        return dataList;
    }

    public void setDataList(List<WeatherDM> list) {
        this.dataList = new ArrayList<>(list);
        notifyDataSetChanged();
    }

    @Override
    public BookmarksAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (layoutInflater == null)
            layoutInflater = LayoutInflater.from(parent.getContext());
        BookmarkRowBinding binding = DataBindingUtil.inflate(layoutInflater,
                R.layout.bookmark_row, parent, false);

        return new BookmarksAdapter.ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(BookmarksAdapter.ViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        //lastVisiblePosition=((LinearLayoutManager)recyclerView.getLayoutManager()).findLastCompletelyVisibleItemPosition();
        lastVisiblePosition = 3;
    }

    @Override
    public int getItemCount() {
        return dataList == null ? 0 : dataList.size();
    }

    public List<WeatherDM> getData() {
        return dataList;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class ViewHolder extends BaseHolder {

        private BookmarkRowBinding binding;

        public ViewHolder(BookmarkRowBinding binding) {
            super(binding.getRoot(), BookmarksAdapter.this.lastVisiblePosition);
            this.binding = binding;
        }

        @Override
        public void bind(int position) {
            super.bind(position);
            binding.setDataModel(dataList.get(position));
            binding.setIsArabic(Constants.isArabic);
            binding.delete.setOnClickListener(this::onDeleteClicked);
            binding.getRoot().setOnClickListener(this::onRowClickedClicked);
        }

        private void onDeleteClicked(View v) {
            homeHandler.deleteItem(getAdapterPosition());
        }

        private void onRowClickedClicked(View v) {
            WeatherDetailsActivity.start(context, dataList.get(getAdapterPosition()));
        }
    }


}

