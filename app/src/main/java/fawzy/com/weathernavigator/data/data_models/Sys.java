package fawzy.com.weathernavigator.data.data_models;

import android.os.Parcel;
import android.os.Parcelable;

import fawzy.com.weathernavigator.utils.DateFormatUtil;

/**
 * Created by fawzy on 1/25/19.
 */

public class Sys implements Parcelable {

    @SuppressWarnings("unused")
    public static final Creator<Sys> CREATOR = new Creator<Sys>() {
        @Override
        public Sys createFromParcel(Parcel in) {
            return new Sys(in);
        }

        @Override
        public Sys[] newArray(int size) {
            return new Sys[size];
        }
    };
    private int type;
    private int id;
    private float message;
    private String country;
    private int sunrise;
    private int sunset;

    public Sys() {
    }

    private Sys(Parcel in) {
        type = in.readInt();
        id = in.readInt();
        message = in.readFloat();
        country = in.readString();
        sunrise = in.readInt();
        sunset = in.readInt();
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getMessage() {
        return message;
    }

    public void setMessage(float message) {
        this.message = message;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getSunrise() {
        return DateFormatUtil.formatLongDate(Long.valueOf(sunrise), "dd MMM yyyy - hh:mm");
    }

    public void setSunrise(int sunrise) {
        this.sunrise = sunrise;
    }

    public String getSunset() {
        return DateFormatUtil.formatLongDate(Long.valueOf(sunset), "dd MMM yyyy - hh:mm");
    }

    public void setSunset(int sunset) {
        this.sunset = sunset;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(type);
        dest.writeInt(id);
        dest.writeFloat(message);
        dest.writeString(country);
        dest.writeInt(sunrise);
        dest.writeInt(sunset);
    }

}
