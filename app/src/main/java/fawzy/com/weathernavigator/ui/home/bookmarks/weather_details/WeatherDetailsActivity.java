package fawzy.com.weathernavigator.ui.home.bookmarks.weather_details;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;

import java.util.Calendar;

import javax.inject.Inject;

import fawzy.com.weathernavigator.AppClass;
import fawzy.com.weathernavigator.R;
import fawzy.com.weathernavigator.data.data_models.WeatherDM;
import fawzy.com.weathernavigator.databinding.WeatherDetailsActBinding;
import fawzy.com.weathernavigator.ui.base.BaseActivity;
import fawzy.com.weathernavigator.utils.DateFormatUtil;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;

/**
 * Created by fawzy on 1/25/19.
 */

public class WeatherDetailsActivity extends BaseActivity {
    @Inject
    SharedPreferences preferences;
    private WeatherDetailsActBinding binding;
    private WeatherDM weatherDM;

    public static void start(Context context, WeatherDM weatherDM) {
        Intent intent = new Intent(context, WeatherDetailsActivity.class);
        intent.putExtra("weatherDM", weatherDM);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkExtras();
        initDI();
        setLocality(preferences);
        renderView();
    }

    private void checkExtras() {
        weatherDM = getIntent().getParcelableExtra("weatherDM");
        if (weatherDM != null)
            weatherDM.setCurrentTime(DateFormatUtil.formatLongDate(Calendar.getInstance().getTimeInMillis(), "hh:mm"));
    }


    private void initDI() {
        ((AppClass) getApplication()).getAppComponent().inject(this);
    }

    protected void renderView() {
        binding = DataBindingUtil.setContentView(this, R.layout.weather_details_act);
        binding.setDataModel(weatherDM);
        setUpToolbar(binding.appBar.toolbar, R.string.weatherDetails, true);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }

}


