package fawzy.com.weathernavigator.dagger.module;

import android.content.Context;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ContextModule {

    @Provides
    @Singleton
    public Context context(@Named("context") Context context) {
        return context.getApplicationContext();
    }
}