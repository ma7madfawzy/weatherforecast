package fawzy.com.weathernavigator.utils;

import android.app.Activity;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by fawzy on 1/25/19.
 */

public class AssetsUtil {
    public static String getData(Activity activity, String data) {
        String json = null;
        try {
            InputStream is = activity.getAssets().open("mockdata" + File.separator + data + ".json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

}
