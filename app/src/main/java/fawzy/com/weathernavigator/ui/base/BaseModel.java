package fawzy.com.weathernavigator.ui.base;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import fawzy.com.weathernavigator.BR;
import fawzy.com.weathernavigator.constants.Constants;
import fawzy.com.weathernavigator.data.Resource;

/**
 * Created by fawzy on 1/25/19.
 */

public class BaseModel extends BaseObservable {
    private Resource resource;

    @Bindable
    public Resource getResource() {
        return resource;
    }

    public void setResource(Resource resource) {
        this.resource = resource;
        notifyPropertyChanged(BR.resource);
    }

    @Bindable
    public boolean isArabic() {
        return Constants.isArabic;
    }


    public void enableLoading() {
        resource = Resource.loading(null);
        setResource(resource);
    }
}

