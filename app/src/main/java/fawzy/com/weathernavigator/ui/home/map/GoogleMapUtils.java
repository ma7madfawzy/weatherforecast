package fawzy.com.weathernavigator.ui.home.map;

import android.content.Context;
import android.graphics.Color;
import android.location.Geocoder;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolygonOptions;

import java.util.List;
import java.util.Locale;

public class GoogleMapUtils {
    /**
     * ZOOM LEVELS
     * 1: World
     * 5: Landmass/continent
     * 10: City
     * 15: Streets
     * 20: Buildings
     **/
    public static final float ZOOM_TO_WORLD = 1;
    public static final float ZOOM_TO_COUNTRY = 5;
    public static final float ZOOM_TO_CITY = 10;
    public static final float ZOOM_BETWEEN_CITY_AND_STREETS = 12;
    public static final float ZOOM_TO_STREETs = 15;
    public static final float ZOOM_TO_Buildings = 20;


    private final Context context;
    private GoogleMap googleMap;
    private boolean restrictMap;

    public GoogleMapUtils(GoogleMap googleMap, Context context) {
        this.googleMap = googleMap;
        this.context = context;
    }

    public static String getStreetName(Context context, LatLng latLng) {
        try {
            double lat = latLng.latitude;
            double lng = latLng.longitude;
            Geocoder geocoder;
            geocoder = new Geocoder(context, Locale.getDefault());
            return String.valueOf(geocoder.getFromLocation(lat, lng, 1).get(0).getAddressLine(0)); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "empty";
    }

    public void drawCircle(LatLng point) {
        // Instantiating CircleOptions to draw a circle around the marker
        CircleOptions circleOptions = new CircleOptions();
        // Specifying the center of the circle
        circleOptions.center(point);
        // Radius of the circle
        circleOptions.radius(500);
        // Border color of the circle
        circleOptions.strokeColor(Color.BLACK);
        // Fill color of the circle
        circleOptions.fillColor(Color.parseColor("#223d7daf"));
        // Border width of the circle
        circleOptions.strokeWidth(2);
        // Adding the circle to the GoogleMap
        googleMap.addCircle(circleOptions);
    }

    private LatLng computeCentroid(List<LatLng> points) {
        double latitude = 0;
        double longitude = 0;
        int n = points.size();

        for (LatLng point : points) {
            latitude += point.latitude;
            longitude += point.longitude;
        }

        return new LatLng(latitude / n, longitude / n);
    }

    public void addMarkers(List<LatLng> lstLatLngRoute) {
        for (LatLng latLngPoint : lstLatLngRoute) {
            MarkerOptions options = new MarkerOptions();
            options.position(latLngPoint);
            googleMap.addMarker(options);
        }
    }

    public Marker setLocationWithAnimate_Zoom(LatLng location, float zoom) {
        if (location == null) return null;

        Marker marker = addMarker(location);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location, zoom));
        return marker;
    }

    public void setMaxMinZoom(float min, float max) {//min like 6.0f , max like 14.0f
        googleMap.setMinZoomPreference(min);
        googleMap.setMaxZoomPreference(max);

    }

    public void dispatchScrollview(View layout, OnMapTouchListener mListener) {// to adjust map scrolling within scrollview
        TouchableWrapper frameLayout = new TouchableWrapper(context, mListener);
        frameLayout.setBackgroundColor(context.getResources().getColor(android.R.color.transparent));
        ((ViewGroup) layout).addView(frameLayout,
                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
    }

    public Marker updateMarker(LatLng position, Marker modelMarker) {
        if (position == null) return modelMarker;
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(position);
        if (modelMarker == null) {
            modelMarker = googleMap.addMarker(markerOptions);
        }
        modelMarker.setPosition(position);
        return modelMarker;
    }

    public Marker addMarker(LatLng position) {
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(position);
        Marker marker = googleMap.addMarker(markerOptions);
        return marker;
    }

    public LatLngBounds highlightArea(List<LatLng> lstLatLngRoute, int fillColor, float strokeWidth, boolean restrictMap) {
        this.restrictMap = restrictMap;
        PolygonOptions polygonOptions = new PolygonOptions();
        polygonOptions.fillColor(fillColor);
        polygonOptions.addAll(lstLatLngRoute);
        polygonOptions.strokeWidth(strokeWidth);
        googleMap.addPolygon(polygonOptions);
        if (restrictMap)
            return restrictMap(lstLatLngRoute);
        return null;
    }

    /**
     * disable map scrolling outside a specific bounds
     **/
    private LatLngBounds restrictMap(List<LatLng> lstLatLngRoute) {
        LatLngBounds bounds = getLatLngBounds(lstLatLngRoute);
        googleMap.setLatLngBoundsForCameraTarget(bounds);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 0));
        return bounds;
    }

    private LatLngBounds getLatLngBounds(List<LatLng> lstLatLngRoute) {
        LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
        for (LatLng latLngPoint : lstLatLngRoute) {
            boundsBuilder.include(latLngPoint);
        }
        return boundsBuilder.build();
    }

    private void centeringMap(List<LatLng> lstLatLngRoute, float zoomLevel, int duration) {
        //calculating center of the LatLng list
        LatLng center = computeCentroid(lstLatLngRoute);
        //defining centered marker
        MarkerOptions options = new MarkerOptions();
        options.position(center);
        moveToLocation(center, zoomLevel, duration);
    }

    private void moveToLocation(LatLng latLng, float zoomLevel, int duration) {
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 0));
        // Zoom in, animating the camera.
        googleMap.animateCamera(CameraUpdateFactory.zoomIn());
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(zoomLevel), duration, null);
    }

    public interface OnMapTouchListener {
        public abstract void onTouch();
    }

    class TouchableWrapper extends FrameLayout {

        private OnMapTouchListener mListener;

        public TouchableWrapper(Context context, OnMapTouchListener listener) {
            super(context);
            mListener = listener;
        }

        @Override
        public boolean dispatchTouchEvent(MotionEvent event) {
            if (mListener == null)
                return super.dispatchTouchEvent(event);
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    mListener.onTouch();
                    break;
                case MotionEvent.ACTION_UP:
                    mListener.onTouch();
                    break;
            }
            return super.dispatchTouchEvent(event);
        }
    }


}
